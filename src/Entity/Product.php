<?php

declare(strict_types = 1);

namespace Gwo\Recruitment\Entity;

class Product
{
    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /** @var int */
    private $unitPrice;

    /** @var int */
    private $minimumQuantity;

    public function __construct()
    {
        $this->minimumQuantity = 1;
    }

    /**
     * @param int $id
     * @return Product
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @return Product
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param int $unitPrice
     * @return Product
     */
    public function setUnitPrice(int $unitPrice): self
    {
        if ($unitPrice <= 0) {
            throw new \InvalidArgumentException('Price must be greater than zero');
        }

        $this->unitPrice = $unitPrice;
        return $this;
    }

    /**
     * @return int
     */
    public function getUnitPrice(): ?int
    {
        return $this->unitPrice;
    }

    /**
     * @param int $minimumQuantity
     * @return Product
     */
    public function setMinimumQuantity(int $minimumQuantity): self
    {
        if ($minimumQuantity <= 0) {
            throw new \InvalidArgumentException('Minimum quantity must be greater than zero');
        }

        $this->minimumQuantity = $minimumQuantity;
        return $this;
    }

    /**
     * @return int
     */
    public function getMinimumQuantity(): ?int
    {
        return $this->minimumQuantity;
    }
}
