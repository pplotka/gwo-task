<?php

declare(strict_types = 1);

namespace Gwo\Recruitment\Cart;

use Gwo\Recruitment\Cart\Exception\QuantityTooLowException;
use Gwo\Recruitment\Entity\Product;

class Item
{
    /** @var Product */
    private $product;

    /** @var int */
    private $quantity;

    /**
     * @param Product $product
     * @param int $quantity
     * @throws QuantityTooLowException
     */
    public function __construct(Product $product, int $quantity)
    {
        $this->checkMinimumQuantity($product, $quantity);
        $this->product = $product;
        $this->quantity = $quantity;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @throws QuantityTooLowException
     */
    public function setQuantity(int $quantity): void
    {
        $this->checkMinimumQuantity($this->product, $quantity);
        $this->quantity = $quantity;
    }

    /**
     * @return int
     */
    public function getTotalPrice(): int
    {
        return $this->product->getUnitPrice() * $this->quantity;
    }

    /**
     * @param Product $product
     * @param int $quantity
     * @throws QuantityTooLowException
     */
    private function checkMinimumQuantity(Product $product, int $quantity): void
    {
        if ($product->getMinimumQuantity() > $quantity) {
            throw new QuantityTooLowException($product, $quantity);
        }
    }
}
