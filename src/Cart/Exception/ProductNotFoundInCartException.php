<?php

declare(strict_types = 1);

namespace Gwo\Recruitment\Cart\Exception;

use Gwo\Recruitment\Entity\Product;

class ProductNotFoundInCartException extends \Exception
{
    public function __construct(Product $product)
    {
        parent::__construct(sprintf(
            'Product with id %u does not exist in Cart',
            $product->getId()
        ));
    }
}
