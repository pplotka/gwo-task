<?php

declare(strict_types = 1);

namespace Gwo\Recruitment\Cart\Exception;

use Gwo\Recruitment\Entity\Product;

class QuantityTooLowException extends \Exception
{
    public function __construct(Product $product, int $quantity)
    {
        parent::__construct(sprintf(
            'Minimum quantity for product is equal %u, and you set %u',
            $product->getMinimumQuantity(),
            $quantity
        ));
    }
}
