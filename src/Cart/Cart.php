<?php

declare(strict_types = 1);

namespace Gwo\Recruitment\Cart;

use Gwo\Recruitment\Cart\Exception\ProductNotFoundInCartException;
use Gwo\Recruitment\Cart\Exception\QuantityTooLowException;
use Gwo\Recruitment\Entity\Product;

class Cart
{
    /** @var Item[] */
    private $items;

    public function __construct()
    {
        $this->items = [];
    }

    /**
     * @return Item[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param int $index
     * @return Item
     */
    public function getItem(int $index): Item
    {
        $this->checkIfExist($index);
        return $this->items[$index];
    }

    /**
     * @param Product $product
     * @param int $quantity
     * @return Cart
     * @throws Exception\QuantityTooLowException
     */
    public function addProduct(Product $product, int $quantity): self
    {
        if ($this->hasProduct($product)) {
            $this->increaseQuantity($product, $quantity);
        } else {
            $item = new Item($product, $quantity);
            $this->items[] = $item;
        }

        return $this;
    }

    /**
     * @param Product $product
     * @return Cart
     */
    public function removeProduct(Product $product): self
    {
        try {
            $index = $this->getIndexForProduct($product);
            array_splice($this->items, $index, 1);
        } catch (ProductNotFoundInCartException $ex) {
        }

        return $this;
    }

    /**
     * @param Product $product
     * @param int $quantity
     * @return Cart
     * @throws QuantityTooLowException
     */
    public function setQuantity(Product $product, int $quantity): self
    {
        try {
            $index = $this->getIndexForProduct($product);
            $this->items[$index]->setQuantity($quantity);
        } catch (ProductNotFoundInCartException $ex) {
            $this->addProduct($product, $quantity);
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getTotalPrice(): int
    {
        $totalPrice = 0;
        foreach ($this->items as $item) {
            $totalPrice += $item->getTotalPrice();
        }

        return $totalPrice;
    }

    /**
     * @param Product $product
     * @return bool
     */
    private function hasProduct(Product $product): bool
    {
        try {
            $this->getIndexForProduct($product);
            return true;
        } catch (ProductNotFoundInCartException $ex) {
        }

        return false;
    }

    /**
     * @param Product $product
     * @param int $quantity
     * @return Cart
     * @throws QuantityTooLowException
     */
    private function increaseQuantity(Product $product, int $quantity): self
    {
        $index = $this->getIndexForProduct($product);
        $item = $this->items[$index];
        $item->setQuantity($item->getQuantity() + $quantity);

        return $this;
    }

    /**
     * @param int $index
     */
    private function checkIfExist(int $index): void
    {
        if (!isset($this->items[$index])) {
            throw new \OutOfBoundsException(sprintf(sprintf('Item for index %u doest not exist', $index)));
        }
    }

    /**
     * @param $product
     * @return int
     * @throws ProductNotFoundInCartException
     */
    private function getIndexForProduct(Product $product): int
    {
        foreach ($this->items as $index => $item) {
            if ($item->getProduct()->getId() === $product->getId()) {
                return $index;
            }
        }

        throw new ProductNotFoundInCartException($product);
    }
}
